#include <stdio.h>

#include "fdc_int.h"

// Trivial problem, to measure bootstrap times

#ifndef MAX_VALUE
#  define MAX_VALUE 1023	// as long as it isn't too small
#endif


int main(int argc, char *argv[])
{
  int arg = 1;
  int solutions = 0, one_solution = 1;

  fd_int v;
  int max_value = MAX_VALUE;

  fd_init(&argc, &argv);

  if (arg < argc && !strcmp(argv[arg], "--all"))
    {
      one_solution = 0;
      arg++;
    }

  if (arg < argc)
    max_value = atoi(argv[arg++]);

  v = fd_new(0, max_value);

  fd_sum(&v, 1, max_value + 1);

  while (fd_solve() == FD_OK)
    {
      printf("solution %d:\n", ++solutions);

      printf ("v = "); fd_print(v); putchar('\n');

#if !(defined(LOCAL_SEARCH) || defined(DISTRIBUTED_SOLVER))
      if (one_solution)
#endif
	break;
    }

  if (solutions)
    printf("%d solutions found\n", solutions);
  else
    printf("inconsistent CSP\n");

  fd_end();

  return !solutions;
}
