#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <paccs.h>

#define MAX_N 512

main(int argc, char *argv[])
{
  fd_int vs[MAX_N];
  int N = 9;
  int solutions = 0, one_solution = 1;
  int i;

  fd_init(&argc, &argv);

  for (i = 1; i < argc; ++i)
    if (!strcmp(argv[i], "--all"))
      one_solution = 0;
    else
      N = atoi(argv[i]);

  if (N > MAX_N)
    fd__fatal("N > MAX_N");

  for (i = 0; i < N; ++i)
    vs[i] = fd_new(0, N - 1);

  for (i = 0; i < N; ++i)
    fd_exactly_var(vs, N, vs[i], i);

  fd_sum(vs, N, N);

  while (fd_solve() == FD_OK)
    {
      printf("solution %d:\n", ++solutions);

      for (i = 0; i < N; ++i)
	{
	  fd_print(vs[i]);
	  putchar(' ');
	}
      putchar('\n');

#if !(defined(LOCAL_SEARCH) || defined(DISTRIBUTED_SOLVER))
      if (one_solution)
#endif
	break;
    }

  if (solutions)
    printf("%d solutions found\n", solutions);
  else
    printf("inconsistent CSP\n");

  fd_end();

  return !solutions;
}
