#include <stdio.h>
#include <fcntl.h>
#include <errno.h>

#include "fdc_int.h"

#ifndef MAX_VARIABLES
#  define MAX_VARIABLES 256		// maximum number of variables
#endif

#define MAX_MAX_VALUE 63		// maximum maximum domain value

static int NV = 8;			// actual number of variables
static int MV = 7;			// actual maximum domain value

int main(int argc, char *argv[])
{
  fd_int vars[MAX_VARIABLES];
  int solutions = 0, one_solution = 1;
  int i;

  fd_init(&argc, &argv);

  for (i = 1; i < argc; ++i)
    if (!strcmp(argv[i], "--all"))
      one_solution = 0;
    else
      NV = atoi(argv[i]);

  if (NV > MAX_VARIABLES)
    fd__fatal("more than MAX_VARIABLES variables requested");

  for (i = 0; i < NV; ++i)
    vars[i] = fd_new(0,  MV);

  while (fd_solve() == FD_OK)
    {
      printf("solution %d:\n", ++solutions);

      for (i = 0; i < NV; ++i)
	{
	  fd_print(vars[i]);
	  putchar(' ');
	}
      putchar('\n');

#if !(defined(LOCAL_SEARCH) || defined(DISTRIBUTED_SOLVER))
      if (one_solution)
#endif
	break;
    }

  if (solutions)
    printf("%d solutions found\n", solutions);
  else
    printf("inconsistent CSP\n");

  fd_end();

  return !solutions;
}
