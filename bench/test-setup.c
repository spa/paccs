#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "fdc_int.h"


int main(int argc, char *argv[])
{
  fd_init(&argc, &argv);

  return fd_solve_setup();
}


int fd_solve_setup()
{
#ifdef DISTRIBUTED_SOLVER
#  ifdef SPLITGO
  return _fd_dsolve_setup();
#  else
#    error "not ready to measure setup time of this distributed solver"
#  endif
#else
  return 0;
#endif
}

#ifdef SPLITGO
#ifdef SPLITGO_MPI
#include <mpi.h>
#endif

#define MAX_AGENTS 256

int _fd_agent_setup()
{
  return 0;
}

int _fd_dsolve_setup()
{
  int tid;
  pthread_t threads[MAX_AGENTS];
  int nagents = 4;
  int i;
  int processes, live_procs;
  char *s;

  if (s = getenv("FDC_AGENTS"))
    {
      int n = atoi(s);

      if (0 <= n && n <= MAX_AGENTS)
	nagents = n;
    }

#ifdef SPLITGO_MPI
  MPI_Comm_rank(MPI_COMM_WORLD, &tid);

#if 0
  if (tid == 0)
    MPI_Comm_size(MPI_COMM_WORLD, &processes);
#endif
#endif

//  _fd_init_store_depository(nagents);

  for (i = 0; i < nagents; ++i)
    pthread_create(&threads[i], NULL, (void *) _fd_agent_setup, NULL);

#if 0
#ifdef SPLITGO_MPI
  if (tid == 0)
    {
      live_procs = processes;

      while (--live_procs)
	MPI_Recv(NULL, 0, MPI_CHAR, MPI_ANY_SOURCE, 0xdead, MPI_COMM_WORLD,
		 MPI_STATUS_IGNORE);
    }
  else
    MPI_Send(NULL, 0, MPI_CHAR, 0, 0xdead, MPI_COMM_WORLD);
#endif
#endif

  for (i = 0; i < nagents; ++i)
    pthread_cancel(threads[i]);

#ifdef SPLITGO_MPI
//  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Finalize();
#endif

  return FD_OK;
}
#endif /* SPLITGO */
