#include <stdio.h>
#include <string.h>

#include <paccs.h>

#define N 3

int main(int argc, char *argv[])
{
  fd_int vs[N], cardinal;
  int solutions = 0, one_solution = 1;
  int i;

  fd_init(&argc, &argv);

  for (i = 1; i < argc; ++i)
    if (!strcmp(argv[i], "--all"))
      one_solution = 0;

  for (i = 0; i < N; ++i)
    vs[i] = fd_new(1, N);

  cardinal = fd_new(0,N);

  fd_exactly_var(vs, N, cardinal, 2);
//  fd_exactly(vs, N, 1, 1);
//  fd_exactly(vs, N, 1, 3);

  while (fd_solve() == FD_OK)
    {
      printf("solution %d:\n", ++solutions);

      for (i = 0; i < N; ++i)
	{
	  fd_print(vs[i]);
	  putchar(' ');
	}

      fd_println(cardinal);

#if !(defined(LOCAL_SEARCH) || defined(DISTRIBUTED_SOLVER))
      if (one_solution)
#endif
	break;
    }

  if (solutions)
    printf("%d solutions found\n", solutions);
  else
    printf("inconsistent CSP\n");

  fd_end();
}
