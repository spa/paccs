#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <paccs.h>

int main(int argc, char *argv[])
{
  fd_int x, y, z;
  int solutions = 0, one_solution = 1;
  int i;

  fd_init(&argc, &argv);

  for (i = 1; i < argc; ++i)
    if (!strcmp(argv[i], "--all"))
      one_solution = 0;

  x = fd_new(1, 4);
  y = fd_new(1, 2);
  z = fd_new(0, 3);

  fd_var_eq_times(x, y, z);

  while (fd_solve() == FD_OK)
    {
      printf("solution %d:\n", ++solutions);

      printf("x = "); fd_println(x);
      printf("y = "); fd_println(y);
      printf("z = "); fd_println(z);

#if !(defined(LOCAL_SEARCH) || defined(DISTRIBUTED_SOLVER))
      if (one_solution)
#endif
	break;
    }

  if (solutions)
    printf("%d solutions found\n", solutions);
  else
    printf("inconsistent CSP\n");

  fd_end();

  return !solutions;
}
