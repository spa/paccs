#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "util.h"

void fd__fatal(char *msg)
{
  fprintf(stderr, "%s\n", msg);
  abort(); // XXX
}

void fd__error(char *format, ...)
{
  va_list args;

  va_start (args, format);
  vfprintf(stderr, format, args);
  va_end (args);
}

void fd__output(char *format, ...)
{
  va_list args;

  va_start (args, format);
  vfprintf(stdout, format, args);
  va_end (args);
}

void fd__info(char *format, ...)
{
  va_list args;

  va_start (args, format);
  vfprintf(stderr, format, args);
  va_end (args);
}

#ifndef fd__debug

void fd__debug(char *format, ...)
{
  va_list args;

  va_start (args, format);
  vfprintf(stderr, format, args);
  va_end (args);
}

#endif
