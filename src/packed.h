
typedef unsigned char byte;

/*
struct _fd_packed_problem {
  int nvariables, nconstraints;
#ifdef USE_OFFSET
  int *variables;		// offset of the variables within data
  int *constraints;		// offset of the constraints within data
#else
  fd_int *variables;		// address of the variables (within data)
  fd_constraint *constraints;	// address of the constraints (within data)
#endif
  byte *data;
};
*/

struct _fd_packed_int {
  int      index;
#ifndef USE_STORE
  fd_value domain;
#endif
  VAR_C_T  *constraints;
  int      nconstraints;
  int      nconnections;

#ifndef DISTRIBUTED_SOLVER
  int      epoch;
  bool     assigned;
  int      flags;
#ifdef USE_VALUE
  int      value;
#endif  
#endif /* DISTRIBUTED_SOLVER */
};

/* fields must correspond to an initial segment of fd_constraint */
struct _fd_packed_constraint {
#if defined(CONSTRAINT_TEMPS) || !defined(DISABLE_ENTAILED)
  int	  index;
#endif
  C_VAR_T *variables;
  int     nvariables;
  int     *constants;
  int     nconstants;
  _fd_constraint_kind kind;
//  int    kind;
};

struct _fd_packed_problem {
  int nvariables, nconstraints;
#ifdef USE_OFFSET
  int *variables;		// offset of the variables within data
  int *constraints;		// offset of the constraints within data
#else
  struct _fd_packed_int **variables;	     // variables' address (within data)
  struct _fd_packed_constraint **constraints; // constraints' address (within data)
#endif
  byte *data;
};
