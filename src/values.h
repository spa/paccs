fd_value fd_new_value(int min, int max);
bool _fd_val_empty(fd_value domain);
int _fd_val_single(fd_value domain, int *value);
void _fd_free_value(fd_value value);
int _fd_val_max(fd_value domain);
int _fd_val_min(fd_value domain);
int _fd_val_del_min(DOMAIN_REF_T(domain));
int _fd_val_del_max(DOMAIN_REF_T(domain));
int _fd_val_contains_val(fd_value domain, int value);
void _fd_val_set_value(DOMAIN_REF_T(domain), int value);
int _fd_val_del_ge(int value, DOMAIN_REF_T(domain));
int _fd_val_del_gt(int value, DOMAIN_REF_T(domain));
int _fd_val_del_le(int value, DOMAIN_REF_T(domain));
int _fd_val_del_lt(int value, DOMAIN_REF_T(domain));
int _fd_val_del_val(int value, DOMAIN_REF_T(domain));
int _fd_val_del_other(DOMAIN_REF_T(domain), int value);
int _fd_val_intersect(DOMAIN_REF_T(domain1), fd_value domain2);
fd_value _fd_val_clone(fd_value value);
void _fd_val_copy(DOMAIN_REF_T(to), fd_value from);
int _fd_val_size(fd_value domain);
void _fd_val_print(fd_value value);

int (*_fd_val_select)(fd_value domain);
int (*_fd_val_del_select)(DOMAIN_REF_T(domain));

fd_iterator _fd_val_iterator(fd_value);
int _fd_val_has_next(fd_iterator);
int _fd_val_next_element(fd_iterator);
void _fd_val_iterator_dispose(fd_iterator);

void _fd_val_set_value(DOMAIN_REF_T(domain), int value);

#ifdef COMPACT_DOMAINS
void _fd_val_split(DOMAIN_REF_T(src), DOMAIN_REF_T(remaining));
#endif

#if !defined(USE_STORE) || !defined(COMPACT_DOMAINS) || defined(INLINE_DOMAINS)
#  define _fd_init_domain(d, l, u) (d = fd_new_value(l, u))
#else
void _fd_init_domain(DOMAIN_REF_T(domain), int min, int max);
#endif

#ifdef INLINE_DOMAINS
#  define _fd_copy_value(v1, v2) ((v1) = (v2))
#else
#  define _fd_copy_value(v1, v2) _fd_val_copy(v1, v2)
#endif
