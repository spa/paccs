#include "fdc_int.h"

#ifdef SPLITGO_MPI
#include <mpi.h>
#endif

#define _str_(s) #s
#define _xstr_(s) _str_(s)

static char *_fd_version =
  "@(#) $Version: " VERSION ", " __DATE__ " " __TIME__ " $";

static char *_fd_options = "@(#) $Options: "

#ifdef COUNT_SOLUTIONS
#warning "COUNT_SOLUTIONS deprecated (4FEB10), use --count-solutions"
#endif

#ifdef SEARCH
#warning "SEARCH removed (on 23DEC13)"
#endif
#ifdef BACK_JUMPING
#warning "BACK_JUMPING removed (on 23JUL14)"
#endif
#ifdef FORWARD_CHECKING
#warning "FORWARD_CHECKING removed (on 23DEC13)"
#endif
#ifdef REVISION_IS_VAR
			   "REVISION_IS_VAR" " "
#endif
#ifdef REVISIONS_LIST
#warning "REVISIONS_LIST deprecated"
			   "REVISIONS_LIST" " "
#endif
#ifdef ORDER_REVISIONS
			   "ORDER_REVISIONS" " "
#endif
#ifdef REVISION_TWO_VARS
#warning "REVISION_TWO_VARS deprecated"
			   "REVISION_TWO_VARS" " "
#endif
#ifdef COUNT_REVISIONS
			   "COUNT_REVISIONS" " "
#endif
#ifdef USE_ENTAILED
#warning "USE_ENTAILED deprecated, subsumed by CONSTRAINT_TEMPS (13APR11)"
#endif
#ifdef FILTER_DOMAINS
			   "FILTER_DOMAINS=" _xstr_(FILTER_DOMAINS) " "
#endif
#ifdef COMPACT_DOMAINS
			   "COMPACT_DOMAINS" " "
#endif
#ifdef INLINE_DOMAINS
			   "INLINE_DOMAINS" " "
#endif
#ifdef DOMAIN_BITS
			   "DOMAIN_BITS=" _xstr_(DOMAIN_BITS) " "
#endif
#ifdef UNIT_BITS
			   "UNIT_BITS=" _xstr_(UNIT_BITS) " "
#endif
#ifdef DOMAIN_BOUNDS
			   "DOMAIN_BOUNDS" " "
#endif
#ifdef USE_VALUE
			   "USE_VALUE" " "
#endif

#ifdef VAL_MAX
#warning "VAL_MAX deprecated (3FEB10), use --val-max"
#endif

#ifdef FIRST_FAIL
#warning "FIRST_FAIL deprecated (3FEB10), use --first-fail"
#endif
#ifdef MOST_CONSTRAINED
#warning "MOST_CONSTRAINED deprecated (3FEB10), use --most-constrained"
#endif
#ifdef SIZE_DEGREE
#warning "SIZE_DEGREE deprecated (3FEB10), use --size-degree"
#endif

#ifdef LOCAL_SEARCH
#warning "LOCAL_SEARCH removed (on 17DEC13)"
#endif

#ifdef DISTRIBUTED_SOLVER
			   "DISTRIBUTED_SOLVER" " "
#endif
#if defined(ABT) || defined(ABT2)
#warning "ABT{,2} removed (on 20DEC13)"
#endif
#ifdef PIPE
#warning "PIPE removed (on 20DEC13)"
#endif
#ifdef DSEARCH
#warning "DSEARCH removed (on 23DEC13)"
#endif
#ifdef SPLITGO
			   "SPLITGO" " "
#endif
#ifdef SPLITGO_MPI
			   "SPLITGO_MPI" " "
#endif

#ifdef SPLIT_EVENLY
#warning "SPLIT_EVENLY deprecated (1FEB10), use --{,team-}split-{even,eager}"
#endif

#ifdef USE_SEM
#warning "USE_SEM deprecated (1JUN09)"
			   "USE_SEM" " "
#endif
#ifdef STEAL_WORK
			   "STEAL_WORK=" _xstr_(STEAL_WORK) " "
#endif

#ifdef CONSTRAINT_CLASS
#warning "CONSTRAINT_CLASS committed (16JUN16)"
#endif
#ifdef CONSTRAINT_TEMPS
			   "CONSTRAINT_TEMPS" " "
#endif
#ifdef DISABLE_ENTAILED
			   "DISABLE_ENTAILED" " "
#endif
#ifdef USE_MATCHING
			   "USE_MATCHING" " "
#endif

#ifdef USE_STORE
			   "USE_STORE" " "
#endif
#ifdef PACK_PROBLEM
			   "PACK_PROBLEM" " "
#endif
#ifdef USE_MMAP
			   "USE_MMAP" " "
#endif

#ifdef GROWABLE_POOL
			   "GROWABLE_POOL" " "
#endif
#ifdef INDEX_IN_POOL
#warning "INDEX_IN_POOL committed (24APR16)"
#endif
#ifdef STORE_IN_POOL
			   "STORE_IN_POOL" " "
#endif
#ifdef NEW_ENTRANCE
			   "NEW_ENTRANCE" " "
#endif
#ifdef CHECK_MOST_WORK
#warning "CHECK_MOST_WORK committed (21DEC10)"
#endif
#ifdef PREFER_NEXT
			   "PREFER_NEXT" " "
#endif
#ifdef DECREMENT_EARLY
			   "DECREMENT_EARLY" " "
#endif
#ifdef FIRST_CANDIDATE
			   "FIRST_CANDIDATE" " "
#endif
#ifdef JOIN_POOL_INDEXES
#warning "JOIN_POOL_INDEXES committed (21DEC10)"
#endif
#ifdef RANDOM_VICTIM
			   "RANDOM_VICTIM" " "
#endif

#ifdef TRY_HARDER
#warning "TRY_HARDER committed (7MAY10)"
#endif
#ifdef ASK_EARLY
			   "ASK_EARLY" " "
#endif
#ifdef ROUND_ROBIN_POLL
			   "ROUND_ROBIN_POLL" " "
#endif

#ifdef ASSIGNED_AFTER	// when sorting, move assigned variables to the end
			   "ASSIGNED_AFTER" " "
#endif

#ifdef STATS_POOL
			   "STATS_POOL" " "
#endif
#ifdef STATS_PROCS
			   "STATS_PROCS" " "
#endif
#ifdef STATS_STEALS
			   "STATS_STEALS" " "
#endif

#ifdef C_VAR_INT
#warning "C_VAR_INT deprecated"
			   "C_VAR_INT" " "
#endif
#ifdef VAR_C_INT
#warning "VAR_C_INT deprecated"
			   "VAR_C_INT" " "
#endif

#ifdef NDEBUG
			   "NDEBUG" " "
#endif

#ifdef OPEN_MPI
			   "OMPI=" _xstr_(OMPI_MAJOR_VERSION) "."
			   _xstr_(OMPI_MINOR_VERSION) "."
			   _xstr_(OMPI_RELEASE_VERSION) " "
#endif

#ifdef __GNUC__
			   "GCC=" _xstr_(__GNUC__) "."
			   _xstr_(__GNUC_MINOR__) "."
			   _xstr_(__GNUC_PATCHLEVEL__) " "
#endif

			   "(" __FILE__ ") "
			   "$";

/* keep _fd_version and _fd_options from being optimised away */

char *fd_version() { return _fd_version; }

char *fd_options() { return _fd_options; }
