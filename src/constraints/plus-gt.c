/* X + Y > k */

int fd_plus_gt_propagate(fd_constraint this, fd_int revise)
{
  fd_int other;
  int other_idx;
  int changed = 0;

  other_idx = revise == VAR(this, 0) ? 1 : 0;
  other = VAR(this, other_idx);

  changed = _fd_var_del_le(this->constants[0] - _fd_var_max(other), revise);

  if (changed && fd_domain_empty(revise))
    return FD_NOSOLUTION;

  // XXX: enqueue further updates here?
  if (changed)
    _fd_revise_connected(this, revise);

  return FD_OK;
}

fd_constraint fd_plus_gt(fd_int x, fd_int y, int k)
{
  fd_constraint c = fd__constraint_new(2, 1);

  if (c)
    {
      c->variables[0] = FD_INT2C_VAR(x); c->variables[1] = FD_INT2C_VAR(y);
      c->constants[0] = k;

      c->kind = FD_CONSTR_PLUS_GT;

      _fd_var_add_constraint(x, c);
      _fd_var_add_constraint(y, c);

      _fd_add_constraint(c);
    }

  return c;
}
