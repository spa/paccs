/* poly-le-k(C, X, k) == sum(C . X) <= k */

static int fd_poly_le_k_filter(fd_constraint this)
{
  int k;
  int min, max;
  int terms = this->nvariables;
  int *mins, *maxs;
  int i;
#ifdef CONSTRAINT_TEMPS
  int *base;

  assert(!fd__constraint_data_valid(this));

  if (!constraint_memory[this->index])
    constraint_memory[this->index] = malloc((2 * terms + 2) * sizeof(int));

  base = constraint_memory[this->index];

  mins = base + 2;
  maxs = mins + terms;
#else
  mins = alloca(terms * sizeof(*mins));
  maxs = alloca(terms * sizeof(*maxs));
#endif

  k = this->constants[terms];

  // sum the minima and the maxima of the terms
  min = max = 0;

  for (i = 0; i < terms; ++i)
    {
      int vl, vh;

      if (this->constants[i] > 0)
	{
	  vl = mins[i] = _fd_var_min(VAR(this, i));
	  vh = maxs[i] = _fd_var_max(VAR(this, i));
	}
      else
	{
	  vl = maxs[i] = _fd_var_max(VAR(this, i));
	  vh = mins[i] = _fd_var_min(VAR(this, i));
	}

      min += this->constants[i] * vl;
      max += this->constants[i] * vh;
    }

  if (min > k)
    return FD_NOSOLUTION;

  if (max <= k)
    {
      fd__constraint_set_entailed(this);
    }
  else if (min == k)
    {
      for (i = 0; i < terms; ++i)
	{
	  int c = this->constants[i];

	  if (c == 0 || mins[i] == maxs[i])  continue;

	  if (c > 0)
	    {
	      _fd_var_del_gt(mins[i], VAR(this, i));

	      // check needed for when variables occur more than once
	      // and with opposite signs
	      if (fd_domain_empty(VAR(this, i)))
		return FD_NOSOLUTION;

	      _fd_revise_connected(this, VAR(this, i));

	      maxs[i] = mins[i];
	    }
	  else
	    {
	      _fd_var_del_lt(maxs[i], VAR(this, i));

	      // check needed for when variables occur more than once
	      // and with opposite signs
	      if (fd_domain_empty(VAR(this, i)))
		return FD_NOSOLUTION;

	      _fd_revise_connected(this, VAR(this, i));

	      mins[i] = maxs[i];
	    }
	}

      max = min;

      fd__constraint_set_entailed(this);
    }
  else // max > k
    {
      // bounds domain filtering

      for (i = 0; i < terms; ++i)
	{
	  int c = this->constants[i];
	  int imin = mins[i];
	  int imax = maxs[i];

	  if (c > 0)
	    {
	      // enforce min(sum{j!=i}(c[j] * x[j])) + c[i] * max[i] <= k

	      if (min + (imax - imin) * c > k)
		{
		  // max[i] = floor((k - min) / c[i]) + min[i]
		  imax = (k - min) / c + imin;

		  _fd_var_del_gt(imax, VAR(this, i));

		  if (fd_domain_empty(VAR(this, i)))
		    return FD_NOSOLUTION;

		  _fd_revise_connected(this, VAR(this, i));

		  max -= (maxs[i] - imax) * c;

		  maxs[i] = imax;
		}
	    }
	  else // c < 0 (does nothing if c is 0)
	    {
	      // enforce min(sum{j!=i}(c[j] * x[j])) + c[i] * min[i] <= k

	      if (min + (imin - imax) * c > k)
		{
		  // min[i] = ceil((k - min) / c[i]) + max[i]
		  imin = (k - min) / c + imax;

		  _fd_var_del_lt(imin, VAR(this, i));

		  if (fd_domain_empty(VAR(this, i)))
		    return FD_NOSOLUTION;

		  _fd_revise_connected(this, VAR(this, i));

		  max -= (mins[i] - imin) * c;

		  mins[i] = imin;
		}
	    }
	}

      if (max <= k)
	fd__constraint_set_entailed(this);
    }

#ifdef CONSTRAINT_TEMPS
  // save values
  *base = min;
  *(base + 1) = max;

  fd__constraint_remember(this);
#endif

  return FD_OK;
}

static int fd_poly_le_k_propagate2(fd_constraint this, fd_int culprit)
{
#ifdef CONSTRAINT_TEMPS
  int k;
  int min, max;
  int terms = this->nvariables;
  int *mins, *maxs;
  int i;
  int *base;
  int x, c;
  int nmin, nmin_x, nmax, nmax_x;

  if (!fd__constraint_data_valid(this))
    return fd_poly_le_k_filter(this);	// ignores culprit

  // bounds filtering

  base = constraint_memory[this->index];

  mins = base + 2;
  maxs = mins + terms;

  min = *base;
  max = *(base + 1);

  k = this->constants[terms];

  // find the (first) term where the culprit appears
  for (x = 0; culprit != VAR(this, x); ++x)
    ;

  nmin_x = _fd_var_min(VAR(this, x));
  nmax_x = _fd_var_max(VAR(this, x));

  if (nmin_x == mins[x] && nmax_x == maxs[x])
    return FD_OK;

  nmin = min;
  nmax = max;

  do
    {
      c = this->constants[x];

      if (c > 0)
	{
	  nmin = nmin + (nmin_x - mins[x]) * c;
	  nmax = nmax - (maxs[x] - nmax_x) * c;
	}
      else if (c < 0)
	{
	  nmin = nmin - (maxs[x] - nmax_x) * c;
	  nmax = nmax + (nmin_x - mins[x]) * c;
	}

      if (nmin > k)
	return FD_NOSOLUTION;

      mins[x] = nmin_x;
      maxs[x] = nmax_x;

      // search for the next term where the culprit appears
      while (++x < terms && culprit != VAR(this, x))
	;
    }
  while (x < terms);

  if (nmin == min && nmax == max)
    return FD_OK;

  if (nmax <= k)
    {
      fd__constraint_set_entailed(this);
    }
  else if (nmin == k)
    {
      for (i = 0; i < terms; ++i)
	{
	  int c = this->constants[i];

	  if (c == 0 || mins[i] == maxs[i])  continue;

	  if (c > 0)
	    {
	      fd_update_domain_and_check(del_gt, mins[i], VAR(this, i));

	      maxs[i] = mins[i];
	    }
	  else
	    {
	      fd_update_domain_and_check(del_lt, maxs[i], VAR(this, i));

	      mins[i] = maxs[i];
	    }
	}

      nmax = nmin;

      fd__constraint_set_entailed(this);
    }
  else if (nmin > min)	// nmax > k
    {
      // bounds domain propagation

      for (i = 0; i < terms; ++i)
	{
	  int c = this->constants[i];
	  int imin = mins[i];
	  int imax = maxs[i];

	  if (c > 0)
	    {
	      // enforce min(sum{j!=i}(c[j] * x[j])) + c[i] * max[i] <= k

	      if (nmin + (imax - imin) * c > k)
		{
		  // max[i] = floor((k - nmin) / c[i]) + min[i]
		  imax = (k - nmin) / c + imin;

		  fd_update_domain_and_check(del_gt, imax, VAR(this, i));

		  nmax -= (maxs[i] - imax) * c;

		  maxs[i] = imax;
		}
	    }
	  else // c < 0 (does nothing if c is 0)
	    {
	      // enforce min(sum{j!=i}(c[j] * x[j])) + c[i] * min[i] <= k

	      if (nmin + (imin - imax) * c > k)
		{
		  // min[i] = ceil((k - nmin) / c[i]) + max[i]
		  imin = (k - nmin) / c + imax;

		  fd_update_domain_and_check(del_lt, imin, VAR(this, i));

		  nmax -= (mins[i] - imin) * c;

		  mins[i] = imin;
		}
	    }
	}

      if (nmax <= k)
	fd__constraint_set_entailed(this);
    }

  *base = nmin;
  *(base + 1) = nmax;

  return FD_OK;
#else /* CONSTRAINT_TEMPS */
  return fd_poly_le_k_filter(this);	// ignores culprit
#endif /* CONSTRAINT_TEMPS */
}

fd_constraint fd_poly_le_k(int cs[], fd_int xs[], int nterms, int k)
{
  fd_constraint c = fd__constraint_new(nterms, nterms + 1);
  int i;

  if (c)
    {
      for (i = 0; i < nterms; ++i)
	c->variables[i] = FD_INT2C_VAR(xs[i]);
      for (i = 0; i < nterms; ++i)
	c->constants[i] = cs[i];
      c->constants[nterms] = k;

      c->kind = FD_CONSTR_POLY_LE_K;

      for (i = 0; i < c->nvariables; ++i)
	_fd_var_add_constraint(VAR(c, i), c);

      _fd_add_constraint(c);
    }

  return c;
}
