/* x = y - z */

static int fd_var_eq_minus_filter(fd_constraint this)
{
  fd_int x, y, z;
  int xmin, xmax, ymin, ymax, zmin, zmax;
  int changed_x, changed_y, changed_z;

  x = VAR(this, 0);
  y = VAR(this, 1);
  z = VAR(this, 2);

  // bounds consistency

  xmin = _fd_var_min(x);
  xmax = _fd_var_max(x);

  zmin = _fd_var_min(z);
  zmax = _fd_var_max(z);

  changed_y = _fd_var_del_lt(xmin + zmin, y) | _fd_var_del_gt(xmax + zmax, y);

  if (changed_y)
    {
      if (fd_domain_empty(y))
	return FD_NOSOLUTION;

      _fd_revise_connected(NULL, y);
    }

#ifndef DISABLE_ENTAILED
  if ((xmin == xmax) + (zmin == zmax) == 2)
    {
      fd__constraint_set_entailed(this);

      return FD_OK;
    }
#endif

  ymin = _fd_var_min(y);
  ymax = _fd_var_max(y);

  changed_z = _fd_var_del_lt(ymin - xmax, z) | _fd_var_del_gt(ymax - xmin, z);

  if (changed_z)
    {
      if (fd_domain_empty(z))
	return FD_NOSOLUTION;

      _fd_revise_connected(NULL, z);

      zmin = _fd_var_min(z);
      zmax = _fd_var_max(z);
    }

  changed_x = _fd_var_del_lt(ymin - zmax, x) | _fd_var_del_gt(ymax - zmin, x);

  if (changed_x)
    {
      if (fd_domain_empty(x))
	return FD_NOSOLUTION;

      _fd_revise_connected(NULL, x);
    }

#ifndef DISABLE_ENTAILED
  if ((ymin == ymax) + (zmin == zmax) == 2)
    fd__constraint_set_entailed(this);
#endif

  return FD_OK;
}

static int fd_var_eq_minus_propagate2(fd_constraint this, fd_int culprit)
{
  fd_int x, y, z;
  int xmin, xmax, ymin, ymax, zmin, zmax;
  int changed_x = 0, changed_y = 0, changed_z = 0;

  x = VAR(this, 0);
  y = VAR(this, 1);
  z = VAR(this, 2);

  // bounds consistency

  if (culprit == x)
    {
      xmin = _fd_var_min(x);
      xmax = _fd_var_max(x);

      changed_y = _fd_var_del_lt(xmin + _fd_var_min(z), y) |
	_fd_var_del_gt(xmax + _fd_var_max(z), y);

      if (changed_y)
	{
	  if (fd_domain_empty(y))
	    return FD_NOSOLUTION;

	  _fd_revise_connected(NULL, y);	// XXX: NULL or this?
	}

      ymin = _fd_var_min(y);
      ymax = _fd_var_max(y);

      changed_z =
	_fd_var_del_lt(ymin - xmax, z) | _fd_var_del_gt(ymax - xmin, z);

      if (changed_z)
	{
	  if (fd_domain_empty(z))
	    return FD_NOSOLUTION;

	  _fd_revise_connected(NULL, z);	// XXX: NULL or this?
	}
    }
  else if (culprit == y)
    {
      ymin = _fd_var_min(y);
      ymax = _fd_var_max(y);

      changed_x = _fd_var_del_lt(ymin - _fd_var_max(z), x) |
	_fd_var_del_gt(ymax - _fd_var_min(z), x);

      if (changed_x)
	{
	  if (fd_domain_empty(x))
	    return FD_NOSOLUTION;

	  _fd_revise_connected(NULL, x);	// XXX: NULL or this?
	}

      xmin = _fd_var_min(x);
      xmax = _fd_var_max(x);

      changed_z =
	_fd_var_del_lt(ymin - xmax, z) | _fd_var_del_gt(ymax - xmin, z);

      if (changed_z)
	{
	  if (fd_domain_empty(z))
	    return FD_NOSOLUTION;

	  _fd_revise_connected(NULL, z);	// XXX: NULL or this?
	}
    }
  else // culprit == z
    {
      zmin = _fd_var_min(z);
      zmax = _fd_var_max(z);

      changed_x = _fd_var_del_lt(_fd_var_min(y) - zmax, x) |
	_fd_var_del_gt(_fd_var_max(y) - zmin, x);

      if (changed_x)
	{
	  if (fd_domain_empty(x))
	    return FD_NOSOLUTION;

	  _fd_revise_connected(NULL, x);	// XXX: NULL or this?
	}

      xmin = _fd_var_min(x);
      xmax = _fd_var_max(x);

      changed_y =
	_fd_var_del_lt(xmin + zmin, y) | _fd_var_del_gt(xmax + zmax, y);

      if (changed_y)
	{
	  if (fd_domain_empty(y))
	    return FD_NOSOLUTION;

	  _fd_revise_connected(NULL, y);	// XXX: NULL or this?
	}

      ymin = zmin + xmin;
      ymax = zmax + xmax;
    }

#ifndef DISABLE_ENTAILED
  if ((xmin == xmax) + (ymin == ymax) == 2)
    fd__constraint_set_entailed(this);
#endif

  return FD_OK;
}

static int fd_var_eq_minus_propagate(fd_constraint this, fd_int revise)
{
  fd_int x, y, z;
  int changed;

  x = VAR(this, 0);
  y = VAR(this, 1);
  z = VAR(this, 2);

  // XXX: just do bounds consistency, for now

  if (revise == x)
    {
      changed =
	_fd_var_del_lt(_fd_var_min(y) - _fd_var_max(z), revise);
      changed |=
	_fd_var_del_gt(_fd_var_max(y) - _fd_var_min(z), revise);
    }
  else if (revise == y)
    {
      changed =
	_fd_var_del_lt(_fd_var_min(x) + _fd_var_min(z), revise);
      changed |=
	_fd_var_del_gt(_fd_var_max(x) + _fd_var_max(z), revise);
    }
  else // revise == z
    {
      changed =
	_fd_var_del_lt(_fd_var_min(y) - _fd_var_max(x), revise);
      changed |=
	_fd_var_del_gt(_fd_var_max(y) - _fd_var_min(x), revise);
    }

  if (changed && fd_domain_empty(revise))
    return FD_NOSOLUTION;

  // XXX: enqueue further updates here?
  if (changed)
    _fd_revise_connected(this, revise);

  return FD_OK;
}

fd_constraint fd_var_eq_minus(fd_int x, fd_int y, fd_int z)
{
  fd_constraint c = fd__constraint_new(3, 0);

  if (c)
    {
      c->variables[0] = FD_INT2C_VAR(x);
      c->variables[1] = FD_INT2C_VAR(y);
      c->variables[2] = FD_INT2C_VAR(z);

      c->kind = FD_CONSTR_VAR_EQ_MINUS;

      _fd_var_add_constraint(x, c);
      _fd_var_add_constraint(y, c);
      _fd_var_add_constraint(z, c);

      _fd_add_constraint(c);
    }

  return c;
}
