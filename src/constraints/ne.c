/* X ne Y */

static int fd_ne_filter(fd_constraint this)
{
  int value;
  int changes = 0;
  int i;

  // only propagate if culprit's domain is a singleton
  for (i = 0; i < 2; ++i)
    if (fd_var_single(VAR(this, i), &value))
      {
	fd_int revise = VAR(this, 1 - i);

	int changed = _fd_var_del_val(value, revise);

	if (changed)
	  {
	    if (fd_domain_empty(revise))
	      return FD_NOSOLUTION;

	    changes++;

	    _fd_revise_connected(this, revise);
	  }
      }

  if (changes)
    fd__constraint_set_entailed(this);

  return FD_OK;
}

int fd_ne_propagate2(fd_constraint this, fd_int culprit)
{
  fd_int revise;
  int value;
  int changed;

  // only propagate if culprit's domain is a singleton
  if (!fd_var_single(culprit, &value))
    return FD_OK;

  revise = VAR(this, culprit == VAR(this, 0) ? 1 : 0);

  changed = _fd_var_del_val(value, revise);

  if (changed)
    {
      if (fd_domain_empty(revise))
	return FD_NOSOLUTION;

      _fd_revise_connected(this, revise); // XXX
    }

  fd__constraint_set_entailed(this);

  return FD_OK;
}

int fd_ne_propagate(fd_constraint this, fd_int revise)
{
  fd_int other;
  int value;
  int changed = 0;

  other = VAR(this, revise == VAR(this, 0) ? 1 : 0);

  // only propagate if other's domain is a singleton
  if (!fd_var_single(other, &value))
    return FD_OK;

  changed = _fd_var_del_val(value, revise);

  if (changed && fd_domain_empty(revise))
    return FD_NOSOLUTION;

  fd__constraint_set_entailed(this);

  // XXX: enqueue further updates here?
  if (changed)
    _fd_revise_connected(this, revise);

  return FD_OK;
}

fd_constraint fd_ne(fd_int x, fd_int y)
{
  fd_constraint c = fd__constraint_new(2, 0);

  if (c)
    {
      c->variables[0] = FD_INT2C_VAR(x); c->variables[1] = FD_INT2C_VAR(y);

      c->kind = FD_CONSTR_NE;

      _fd_var_add_constraint(x, c);
      _fd_var_add_constraint(y, c);

      _fd_add_constraint(c);
    }

  return c;
}
