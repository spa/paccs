/* X gt Y == Y lt X */

fd_constraint fd_gt(fd_int x, fd_int y)
{
  return fd_lt(y, x);
}
