/* X ge Y == Y le X */

fd_constraint fd_ge(fd_int x, fd_int y)
{
  return fd_le(y, x);
}
