/* poly-ne-k(C, X, k) == sum(C . X) != k */

static int fd_poly_ne_k_filter(fd_constraint this)
{
  int k;
  int min, max;
  int terms = this->nvariables;
  int *mins, *maxs;
  int unset = 0;		// non-singleton variables
  int i;
#ifdef CONSTRAINT_TEMPS
  int *base;

  assert(!fd__constraint_data_valid(this));

  if (!constraint_memory[this->index])
    constraint_memory[this->index] = malloc((2 * terms + 3) * sizeof(int));

  base = constraint_memory[this->index];

  mins = base + 3;
  maxs = mins + terms;
#else
  mins = alloca(terms * sizeof(*mins));
  maxs = alloca(terms * sizeof(*maxs));
#endif

  k = this->constants[terms];

  // sum the minima and the maxima of the terms
  min = max = 0;

  for (i = 0; i < terms; ++i)
    {
      int vl, vh;
      int c = this->constants[i];

      if (c > 0)
	{
	  vl = mins[i] = _fd_var_min(VAR(this, i));
	  vh = maxs[i] = _fd_var_max(VAR(this, i));
	}
      else
	{
	  vl = maxs[i] = _fd_var_max(VAR(this, i));
	  vh = mins[i] = _fd_var_min(VAR(this, i));
	}

      if (c && vl != vh)
	unset++;

      min += c * vl;
      max += c * vh;
    }

  if (min > k || max < k)
    {
      fd__constraint_set_entailed(this);

      return FD_OK;
    }

  if (min == max)
    {
      if (min == k)
	return FD_NOSOLUTION;

      fd__constraint_set_entailed(this);

      return FD_OK;
    }

  if (unset == 1)
    {
      int m, v;

      // find out which is the non-singleton variable
      for (i = terms - 1; i >= 0; --i)
	if (this->constants[i] && mins[i] != maxs[i])
	  break;

      if (this->constants[i] > 0)
	m = min - this->constants[i] * mins[i];
      else
	m = min - this->constants[i] * maxs[i];

      v = (k - m) / this->constants[i];

      if (v * this->constants[i] + m == k)
	fd_update_domain_and_check(del_val, v, VAR(this, i));

      fd__constraint_set_entailed(this);
    }

#ifdef CONSTRAINT_TEMPS
  // save values
  *base = min;
  *(base + 1) = max;
  *(base + 2) = unset;

  fd__constraint_remember(this);
#endif

  return FD_OK;
}

static int fd_poly_ne_k_propagate2(fd_constraint this, fd_int culprit)
{
#ifdef CONSTRAINT_TEMPS
  int k;
  int min, max;
  int terms = this->nvariables;
  int *mins, *maxs;
  int unset;
  int i;
  int *base;
  int x, c;
  int nmin, nmin_x, nmax, nmax_x;

  if (!fd__constraint_data_valid(this))
    return fd_poly_ne_k_filter(this);	// ignores culprit

  // bounds filtering

  base = constraint_memory[this->index];

  mins = base + 3;
  maxs = mins + terms;

  k = this->constants[terms];

  min = *base;
  max = *(base + 1);

  unset = *(base + 2);

  // the culprit appears in one of the terms, find out which one(s)
  for (x = 0; culprit != VAR(this, x); ++x)
    ;

  nmin_x = _fd_var_min(VAR(this, x));
  nmax_x = _fd_var_max(VAR(this, x));

  if (nmin_x == mins[x] && nmax_x == maxs[x])
    return FD_OK;

  nmin = min;
  nmax = max;

  do
    {
      c = this->constants[x];

      if (c && nmin_x == nmax_x)
	unset--;

      if (c > 0)
	{
	  nmin = nmin + (nmin_x - mins[x]) * c;
	  nmax = nmax - (maxs[x] - nmax_x) * c;
	}
      else if (c < 0)
	{
	  nmin = nmin - (maxs[x] - nmax_x) * c;
	  nmax = nmax + (nmin_x - mins[x]) * c;
	}

      mins[x] = nmin_x;
      maxs[x] = nmax_x;

      while (++x < terms && culprit != VAR(this, x))
	;
    }
  while (x < terms);

  if (nmin > k || nmax < k)
    {
      fd__constraint_set_entailed(this);

      return FD_OK;
    }

  if (nmin == nmax)
    {
      if (nmin == k)
	return FD_NOSOLUTION;

      fd__constraint_set_entailed(this);

      return FD_OK;
    }

  if (unset == 1)
    {
      int m, v;

      // find out which is the non-singleton variable
      for (i = terms - 1; i >= 0; --i)
	if (this->constants[i] && mins[i] != maxs[i])
	  break;

      // m = sum{j != i} c[j] * x[j]
      if (this->constants[i] > 0)
	m = nmin - this->constants[i] * mins[i];
      else
	m = nmin - this->constants[i] * maxs[i];

      // v * c[i] + m = k (if v were a real value)
      v = (k - m) / this->constants[i];

      if (v * this->constants[i] + m == k)
	fd_update_domain_and_check(del_val, v, VAR(this, i));

      fd__constraint_set_entailed(this);
    }

  *base = nmin;
  *(base + 1) = nmax;
  *(base + 2) = unset;

  return FD_OK;
#else /* CONSTRAINT_TEMPS */
  return fd_poly_ne_k_filter(this);	// ignores culprit
#endif /* CONSTRAINT_TEMPS */
}

fd_constraint fd_poly_ne_k(int cs[], fd_int xs[], int nterms, int k)
{
  fd_constraint c = fd__constraint_new(nterms, nterms + 1);
  int i;

  if (c)
    {
      for (i = 0; i < nterms; ++i)
	c->variables[i] = FD_INT2C_VAR(xs[i]);
      for (i = 0; i < nterms; ++i)
	c->constants[i] = cs[i];
      c->constants[nterms] = k;

      c->kind = FD_CONSTR_POLY_NE_K;

      for (i = 0; i < c->nvariables; ++i)
	_fd_var_add_constraint(VAR(c, i), c);

      _fd_add_constraint(c);
    }

  return c;
}
