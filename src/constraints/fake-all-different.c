/* fake-all-different */

/* simulates the all-different constraint using the binary `ne' constraint */

fd_constraint fd_fake_all_different(fd_int *variables, int nvariables)
{
  int i, j;

  for (i = 0; i < nvariables; ++i)
    for (j = i + 1; j < nvariables; ++j)
      fd_ne(variables[i], variables[j]);

  return NULL;
}
