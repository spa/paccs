extern int (*fd__split_problem_f)(int, _fd_store[]);
#ifdef SPLITGO_MPI
extern int (*fd__split_team_problem_f)(int, _fd_store[]);
#endif

void fd__init_splitgo(int *argc, char *argv[]);

int fd__split_problem(int, _fd_store[], int (*)(int, _fd_store[]));
