
// XXX: these declarations are also in fdc_int.h; one copy should be removed
extern int fd_variables_count;
extern __thread fd_int _fd_variables[];

extern fd_int *fd__label_vars;
extern int fd__label_vars_count;
extern bool *fd__var_labelled;

fd_int fd_const(int value);
fd_int fd_new(int min, int max);
fd_int _fd_var_copy(fd_int variable);
void _fd_var_copy_domain(fd_int to, fd_int from);
void _fd_var_copy_domains(fd_int to[], fd_int from[]);
void _fd_var_copy_values(fd_int to[], int values[]);
void _fd_var_add_constraint(fd_int variable, fd_constraint constraint);
#ifndef fd_domain_empty
int fd_domain_empty(fd_int variable);
#endif
int fd_var_single(fd_int variable, int *value);
int fd_var_value(fd_int variable);
void _fd_revise_connected(fd_constraint constraint, fd_int variable);
void fd_print(fd_int variable);
void fd_println(fd_int variable);
void _fd_print(void);
void _fd_cprint(void);
void _fd_gprint(void);
int _fd_var_max(fd_int variable);
int _fd_var_min(fd_int variable);
int _fd_var_del_ge(int value, fd_int variable);
int _fd_var_del_gt(int value, fd_int variable);
int _fd_var_del_le(int value, fd_int variable);
int _fd_var_del_lt(int value, fd_int variable);
int _fd_var_del_val(int value, fd_int variable);
int _fd_var_del_other(fd_int variable, int value);
int _fd_var_intersect(fd_int variable1, fd_int variable2);
int _fd_var_contains_val(fd_int variable, int value);
void _fd_var_set_value(fd_int variable, int value);

#define fd_update_domain(op, val, var) \
  do {				       \
    _fd_var_ ## op(val, var);	       \
    				       \
    _fd_revise_connected(this, var);   \
  } while (0)

#define fd_update_domain_and_check(op, val, var) \
  do {						 \
    if (_fd_var_ ## op(val, var))		 \
      {						 \
	if (fd_domain_empty(var))		 \
	  return FD_NOSOLUTION;			 \
						 \
	_fd_revise_connected(this, var);	 \
      }						 \
  } while (0)

// SEARCH
extern int (*fd__cmp_variables)(fd_int, fd_int);
int fd__cmp_var_size(fd_int, fd_int);
int fd__cmp_var_constraints(fd_int, fd_int);
int fd__cmp_var_size_degree(fd_int, fd_int);
int fd__cmp_var_connections(fd_int, fd_int);
int fd__cmp_var_min(fd_int, fd_int);
int fd__cmp_var_max(fd_int, fd_int);

extern fd_int (*_fd_var_select2)(fd_int _fd_variables[]);
fd_int _fd_select_first_var(fd_int _fd_variables[]);
fd_int _fd_select_first_fail(fd_int _fd_variables[]);
fd_int _fd_select_most_constrained(fd_int _fd_variables[]);
fd_int _fd_select_size_degree(fd_int _fd_variables[]);
fd_int _fd_select_most_connected(fd_int _fd_variables[]);
fd_int _fd_select_random_var(fd_int _fd_variables[]);
fd_int _fd_select_min_value(fd_int _fd_variables[]);
fd_int _fd_select_max_value(fd_int _fd_variables[]);
fd_int _fd_var_select(void);
