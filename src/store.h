
#ifndef _STORE_H_
#define _STORE_H_ 1

typedef _fd_value_core *_fd_store;

/* How to obtain an fd_value from the store contents. */
#ifdef INLINE_DOMAINS
#define SVALUE(s) (s)
#else
#define SVALUE(s) (&(s))
#endif

extern __thread _fd_store store;

#endif /* _STORE_H_ */
