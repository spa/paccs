#ifdef CONSTRAINT_TEMPS
int _fd_find_matching(fd_constraint c, int **memory);
int _fd_update_matching(fd_constraint c, fd_int culprit, int *memory);
#else
int _fd_find_matching(fd_constraint c);
#endif
