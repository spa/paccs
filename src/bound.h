#ifndef _BOUND_H
#define _BOUND_H 1

extern bool _fd_optimising;

typedef struct _fd_bound_data _fd_bound_data;

extern _fd_bound_data *_fd_bound;


_fd_bound_data *_fd_init_bound(int, C_VAR_T, fd_constraint,
			       int (*)(fd_constraint, _fd_store, int *),
			       int (*)(int, int),
			       int (*)(fd_constraint, _fd_store, _fd_store),
			       bool (*)(fd_constraint, int));

bool _fd_set_bound(int);
int _fd_bound_value(void);
bool _fd_bound_check_set(_fd_store);
bool _fd_bound_check(_fd_store);
int _fd_better_solution(_fd_store, _fd_store);
fd_int _fd_bound_variable();
int fd__bound_and_revise();

#endif /* _BOUND_H */
