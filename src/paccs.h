/* Types */

typedef void *fd_int;
typedef void *fd_constraint;

/* Constants */

#define FD_OK 0
#define FD_NOSOLUTION 1

/* Functions */

void fd_init(int *argc, char **argv[]);
void fd_end(void);
int fd_solve(void);

fd_int fd_const(int value);
fd_int fd_new(int min, int max);

void fd_label(fd_int vars[], int nvars);
int fd_var_single(fd_int var, int *val);
int fd_var_value(fd_int var);
void fd_print(fd_int var);
void fd_println(fd_int var);

/* Constraints */

/* Arithmetic constraints */

fd_constraint fd_eq(fd_int x, fd_int y);
fd_constraint fd_ne(fd_int x, fd_int y);
fd_constraint fd_lt(fd_int x, fd_int y);
fd_constraint fd_le(fd_int x, fd_int y);
fd_constraint fd_gt(fd_int x, fd_int y);
fd_constraint fd_ge(fd_int x, fd_int y);
fd_constraint fd_minus_eq(fd_int x, fd_int y, int k);
fd_constraint fd_minus_ne(fd_int x, fd_int y, int k);
fd_constraint fd_var_eq_minus(fd_int x, fd_int y, fd_int z);
fd_constraint fd_var_eq_times(fd_int x, fd_int y, fd_int z);

/* Global constraints */

fd_constraint fd_all_different(fd_int X[], int n);
fd_constraint fd_fake_all_different(fd_int X[], int n);

fd_constraint fd_element(fd_int X[], int n, fd_int y, int k);
fd_constraint fd_element_var(fd_int X[], int n, fd_int y, fd_int z);

fd_constraint fd_sum(fd_int X[], int n, int k);
fd_constraint fd_sum2(fd_int X[], int n, fd_int y);

fd_constraint fd_sum_prod(fd_int X[], fd_int Y[], int n, int k);

fd_constraint fd_poly_eq(int C[], fd_int Y[], int n, fd_int z);
fd_constraint fd_poly_eq_k(int C[], fd_int Y[], int n, int k);
fd_constraint fd_poly_le_k(int C[], fd_int Y[], int n, int k);
fd_constraint fd_poly_ne(int C[], fd_int Y[], int n, fd_int y);
fd_constraint fd_poly_ne_k(int C[], fd_int Y[], int n, int k);
fd_constraint fd_knapsack2(fd_int X[], fd_int Y[], int n, fd_int z);

fd_constraint fd_exactly(fd_int X[], int n, int c, int k);
fd_constraint fd_exactly_var(fd_int X[], int n, fd_int y, int k);
fd_constraint fd_exactly_vars(fd_int X[], int n, fd_int y, fd_int z);

/* Optimisation */

fd_constraint fd_min(fd_int x);
fd_constraint fd_max(fd_int x);
