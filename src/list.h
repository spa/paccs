
typedef struct _list *fd_list;
typedef struct node *fd_list_iterator;

fd_list fd_list_new(void);
int fd_list_empty(fd_list);
int fd_list_insert(fd_list, void *);
int fd_list_append(fd_list, void *);
int fd_list_sorted_insert(fd_list, void *, int (*)(void *, void *));
void *fd_list_remove(fd_list);
void *fd_list_head(fd_list);
void *fd_list_tail(fd_list);

void fd_list_iterate(fd_list);
int fd_list_has_next(fd_list);
void *fd_list_next_element(fd_list);

fd_list_iterator fd_list_iterate2(fd_list);
int fd_list_has_next2(fd_list_iterator);
void *fd_list_next_element2(fd_list_iterator *);

void fd_list_dispose(fd_list);
void fd_list_dispose_deep(fd_list, void (*)(void *));
void fd_list_empty_list(fd_list);
void fd_list_empty_list_deep(fd_list, void (*)(void *));

int fd_list_length(fd_list list); // XXX

#if 0
// XXX
typedef struct node *node;

struct _list {
  node head, tail;
  node state; // allows one iterator per list
};

struct node {
  void *element;
  node next;
};
#endif
