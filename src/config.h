
#ifdef __linux__

#define HAVE_WORDSIZE_H 1

#define HAVE_TIMERSUB 1

#define HAVE_FFSL 1

#define HAVE_POSIX_MEMALIGN 1

#if __GNUC__ == 4 && __GNUC_MINOR__ == 1 || __GNUC__ == 3
#ifdef __x86_64
#  define __SIZEOF_LONG__ 8
#  define __SIZEOF_POINTER__ 8
#else
#  define __SIZEOF_LONG__ 4
#  define __SIZEOF_POINTER__ 4
#endif

#define __SIZEOF_INT__ 4
#define __SIZEOF_LONG_LONG__ 8

#endif

#elif defined(__SUNPRO_C)

#ifdef __x86_64
#  define __WORDSIZE 64
#  define __SIZEOF_LONG__ 8
#  define __SIZEOF_POINTER__ 8
#else
#  define __WORDSIZE 32
#  define __SIZEOF_LONG__ 4
#  define __SIZEOF_POINTER__ 4
#endif

#define __SIZEOF_INT__ 4
#define __SIZEOF_LONG_LONG__ 8

#define HAVE_MEMALIGN 1

#else

#error "** What am I being compiled on?"

#endif
