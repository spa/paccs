#ifdef USE_STORE

#include <stdlib.h>

#include "fdc_int.h"
#include "store.h"

__thread _fd_store store;


void _fd_init_main_store()
{
  store = calloc(MAX_VARIABLES, sizeof(*store)); // XXX: NULL
}

#if 0
void _fd_init_store()
{
  int i;

  store = calloc(fd_variables_count, sizeof(*store)); // XXX: NULL

  for (i = 0; i < fd_variables_count; ++i)
    store[i] = _fd_variables[i]->domain;
}
#endif

#endif /* USE_STORE */
