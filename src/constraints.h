
extern int _fd_constraint_count;
extern fd_constraint _fd_constraints[];

#define CONSTRAINT(v,c) _fd_constraints[(v)->constraints[c]]

typedef enum {
  FD_CONSTR_NE = 0,
  FD_CONSTR_EQ,
  FD_CONSTR_LT,
  FD_CONSTR_GT = FD_CONSTR_LT,          // XXX: is this standard C?
  FD_CONSTR_LE,
  FD_CONSTR_GE = FD_CONSTR_LE,          // XXX: is this standard C?
  FD_CONSTR_MINUS_NE,
  FD_CONSTR_MINUS_EQ,
  FD_CONSTR_PLUS_GT,
  FD_CONSTR_VAR_EQ_MINUS,
  FD_CONSTR_ALL_DIFFERENT,
  FD_CONSTR_EXACTLY_ONE,
  FD_CONSTR_NOGOODS,
  FD_CONSTR_EXACTLY,
  FD_CONSTR_EXACTLY_VAR,
  FD_CONSTR_SUM,
  FD_CONSTR_VAR_EQ_TIMES,
  FD_CONSTR_SUM_PROD,
  FD_CONSTR_ELEMENT,
  FD_CONSTR_KNAPSACK2,
  FD_CONSTR_SUM2,
  FD_CONSTR_MIN,
  FD_CONSTR_MAX,
  FD_CONSTR_POLY_EQ,
  FD_CONSTR_ELEMENT_VAR,
  FD_CONSTR_EXACTLY_VARS,
  FD_CONSTR_POLY_EQ_K,
  FD_CONSTR_POLY_NE,
  FD_CONSTR_POLY_NE_K,
  FD_CONSTR_POLY_LE_K,
  FD_CONSTR_KINDS		// number of kinds of constraints
} _fd_constraint_kind;

typedef struct _fd_constraint_class {
  int (*propagator2)(fd_constraint, fd_int);
			  // propagates changes in a variable's
			  // domain; 2nd argument is the variable
			  // against which to revise the domains of
			  // the other variables in the constraint;
			  // returns FD_NOSOLUTION if some domain
			  // becomes empty, and FD_OK otherwise
  int (*filter)(fd_constraint);
			  // performs an initial filtering of the
			  // domains of the constraint variables
} _fd_constraint_class;

extern _fd_constraint_class _fd_constraint_data[];

#define _fd_propagate(c,v) (_fd_constraint_data[(c)->kind].propagator2(c, v))
#define _fd_filter(c) (_fd_constraint_data[(c)->kind].filter(c))

void fd__init_constraints();


#if defined(CONSTRAINT_TEMPS) || !defined(DISABLE_ENTAILED)
#define CONSTRAINTS_HAVE_STATE 1
#endif

#ifdef CONSTRAINTS_HAVE_STATE
void fd__constraint_data_reset(void);
#else
#define fd__constraint_data_reset() ((void) 0)
#endif

#ifndef DISABLE_ENTAILED
int fd__constraint_entailed(fd_constraint);
#else
#define fd__constraint_entailed(_) 0
#endif
