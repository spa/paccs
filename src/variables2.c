#include <stdio.h>
#include <stdlib.h>

#include "fdc_int.h" // XXX
#include "values.h"

extern int fd_variables_count;

// print all variables domains (and their epoch)
void _fd_print2(fd_int variables[])
{
  int i;

  for (i = 0; i < fd_variables_count; ++i)
    {
      fd_print(variables[i]);
      printf("\t(%d)\n", variables[i]->epoch);
    }
}

// print all variables domains (and their epoch) on a single line
void _fd_cprint2(fd_int variables[])
{
  int i;

  for (i = 0; i < fd_variables_count; ++i)
    {
      fd_print(variables[i]);
      putchar(' '); //printf("/%d ", variables[i]->epoch);
    }

  putchar('\n');
}

void _fd_gprint2(fd_int variables[])
{
  int i, v;

  for (i = 0; i < fd_variables_count; ++i)
    {
      i % 9 == 0 ? putchar('\n') : 0; // XXX

      _fd_val_single(DOMAIN(variables[i]), &v);
      printf("%d ", v);
    }

  putchar('\n');
}

#ifdef USE_STORE
// print all variables domains from store on a single line
void _fd_cprint3(_fd_store store)
{
  int i;

  for (i = 0; i < fd_variables_count; ++i)
    {
      _fd_val_print(DOMAIN(_fd_variables[i]));
      putchar(' ');
    }

  putchar('\n');
}

void _fd_cprint4(_fd_store store, int n)
{
  int i;

  for (i = 0; i < n; ++i)
    {
      _fd_val_print(DOMAIN(_fd_variables[i]));
      putchar(' ');
    }

  putchar('\n');
}
#endif
