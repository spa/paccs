#include "misc.h"

#if !defined(HAVE_FFSL) && __SIZEOF_LONG__ != __SIZEOF_INT__
#if __SIZEOF_LONG__ != 8 || __SIZEOF_INT__ != 4
#error "this ffsl only works if long is 64 bits and int is 32"
#endif

#include <strings.h>

int ffsl(long int i)
{
  int b;

  if (b = ffs((int) (i & 0x00000000FFFFFFFFL)))
    return b;

  if (b = ffs((int) (i >> 32)))
    return b + 32;

  return 0;
}
#endif

#ifndef HAVE_POSIX_MEMALIGN
#ifdef HAVE_MEMALIGN
#include <stdlib.h>

int posix_memalign(void **memptr, size_t alignment, size_t size)
{
  *memptr = memalign(alignment, size);

  if (*memptr == NULL)
    return 1;		// XXX: should be EINVAL or ENOMEM

  return 0;
}
#endif
#endif
