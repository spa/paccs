#include "config.h"

#ifndef HAVE_TIMERSUB
#include <sys/time.h>
/* copied from Debian's glibc 2.9-4 /usr/include/sys/time.h */
#define timersub(a, b, result)						      \
  do {									      \
    (result)->tv_sec = (a)->tv_sec - (b)->tv_sec;			      \
    (result)->tv_usec = (a)->tv_usec - (b)->tv_usec;			      \
    if ((result)->tv_usec < 0) {					      \
      --(result)->tv_sec;						      \
      (result)->tv_usec += 1000000;					      \
    }									      \
  } while (0)
#endif

#ifndef HAVE_FFSL
#  if __SIZEOF_LONG__ == __SIZEOF_INT__
#    define ffsl ffs
#  else
int ffsl(long int);
#  endif
#endif

#ifndef HAVE_POSIX_MEMALIGN
int posix_memalign(void **memptr, size_t alignment, size_t size);
#endif
