void fd__fatal(char *msg);
void fd__error(char *format, ...);
void fd__output(char *format, ...);
void fd__info(char *format, ...);

#ifndef fd__debug
void fd__debug(char *format, ...);
#endif
