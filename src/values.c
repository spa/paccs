#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fdc_int.h"
#include "values.h"

#include "util.h"


#ifdef COMPACT_DOMAINS
#  include "values-bitmap.c"
#else
#  include "values-intervals.c"
#endif
