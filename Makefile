SRC = src
VPATH = $(SRC):bench:fz/examples

ARCH = $(shell uname -s)

ifeq ($(ARCH), SunOS)
  CC = cc
  CFLAGS = -g -m64 -xc99
else
  CC = gcc
  CFLAGS = -ggdb3 -O3 # -std=c99 -Wall
endif

CPPFLAGS = -I$(SRC) $(ALL_DEFINES)

# -DFILTER_DOMAINS	perform an initial filtering on the domains
# -DCOMPACT_DOMAINS	use a bitmapped representation of domains
# -DINLINE_DOMAINS	bitmapped domains accessed directly
# -DUSE_VALUE		maintain a field with the value of the variable
# -DUSE_STORE		keep all domains in a compact store
# -DPACK_PROBLEM	use a compact (quasi-relocatable) problem definition
# -DREVISION_IS_VAR	revisions only contain the variable to revise against
# -DORDER_REVISIONS	order the revisions list(s)
#   -DCOUNT_REVISIONS	count and report number of revisions performed
#   -DDEBUG_REVISION	debug the insertion of revisions in their list(s)
# -DDEBUG_SEARCH	applies to all exhaustive searches
# -DDEBUG_VALUE		debug operations on values (domains)

# -DDISTRIBUTED_SOLVER	use one of the parallel/distributed algorithms
#   -DSPLITGO		simultaneous DFSs on disjoint parts of the domain
#			without backtracking
#   -DSPLITGO_MPI	distributed split&go
#     -DBIG		distributed split&go with neighbourhoods (UNAVAILABLE)

EXTRA_DEFINES =

DEFINES = # -DTRACE -DNDEBUG
DEFAULTS = -DREVISION_IS_VAR -DGROWABLE_POOL -DSTORE_IN_POOL \
	   -DNEW_ENTRANCE -DRANDOM_VICTIM -DDOMAIN_BOUNDS
REQUIRED = -DDISTRIBUTED_SOLVER -DSPLITGO -DCOMPACT_DOMAINS -DUSE_STORE \
	   -DPACK_PROBLEM

ALL_DEFINES = $(REQUIRED) $(DEFAULTS) $(DEFINES) $(EXTRA_DEFINES)

# imbed version and svn's revision into the code
ifeq ($(VERSION),)
  ifneq ($(wildcard $(SRC)/VERSION), )
    VERSION = $(shell cat $(SRC)/VERSION)
  else
    $(error could not determine code version (VERSION not set))
  endif
endif
override CPPFLAGS += -DVERSION="\"paccs $(VERSION)\""

FDC_FILES = util.c variables.c problem.c revisions.c options.c bound.c \
	    packed.c store.c misc.c $(FDC_OBJS)

FDC_OBJS = constraints.o list.o values.o

.INTERMEDIATE: constraints.o values.o

ifeq ($(filter -DUSE_MATCHING, $(ALL_DEFINES)), -DUSE_MATCHING)
  FDC_FILES += matching.c
endif

DFDC_FILES = dsearch-sg.c splitting.c variables2.c

AGENTS_C = agents-splitgo.c

ifeq ($(findstring -DBIG, $(DEFINES) $(EXTRA_DEFINES)), -DBIG)
  MPI_AGENTS_C = agents-splitgo-mpi-big.c
else
  MPI_AGENTS_C = agents-splitgo-mpi.c
endif


FDC_SRCS =							\
	 $(subst bench/,,$(wildcard bench/*.c))			\
	 $(subst fz/examples/,,$(wildcard fz/examples/*.c))

FDC_NAMES = $(subst .c,,$(FDC_SRCS))
FDC_BINS = $(FDC_NAMES) x

DFDC_BINS = $(FDC_BINS:=-mpi)


none:
	@echo "choose one from: $(FDC_NAMES) ..."


$(FDC_BINS) : $(FDC_FILES) $(DFDC_FILES) $(AGENTS_C)

$(FDC_BINS) : override DEFAULTS += -DSTEAL_WORK=1
$(FDC_BINS) : override AGENTS_C = agents-splitgo.c
$(FDC_BINS) : override CFLAGS += -pthread


%-mpi: override AGENTS_C = agents-splitgo-mpi.c
%-mpi: override DEFINES += -DSPLITGO_MPI
%-mpi: override DEFAULTS += -DSTEAL_WORK=2
%-mpi: override CC = mpicc
%-mpi: LDLIBS = -lrt

%-mpi: %.c $(FDC_FILES) $(DFDC_FILES) $(MPI_AGENTS_C)
	$(LINK.c) $^ $(LOADLIBES) $(LDLIBS) -o $@


ifeq ($(DEBUG), 1)
  CFLAGS := -g $(CFLAGS)
  CPPFLAGS := -DDEBUG_VALUE -DDEBUG_REVISION -DDEBUG_SEARCH $(CPPFLAGS)
endif


constraints.o: fdc_int.h constraints.h
constraints.o: $(SRC)/constraints/*.c

list.o: $(SRC)/list.c $(SRC)/list.h

values.o: values.h values-bitmap.c values-intervals.c fdc_int.h


queens queens-mpi : override DEFAULTS += -DDISABLE_ENTAILED

costas costas-mpi : override DEFAULTS += -DCONSTRAINT_TEMPS

qap qap-mpi : override DEFAULTS += \
  -DDOMAIN_BITS=256 -DCONSTRAINT_TEMPS -DQAP_MODEL_B

magic-square magic-square-mpi : override DEFAULTS += -DCONSTRAINT_TEMPS

graphs graphs-mpi : override DEFAULTS += -DUSE_MATCHING -DCONSTRAINT_TEMPS
graphs graphs-mpi : matching.c

partition partition-mpi : override DEFAULTS += -DCONSTRAINT_TEMPS

golomb golomb-mpi : override DEFAULTS += -DDOMAIN_BOUNDS -DDOMAIN_BITS=128

langford langford-mpi : override DEFAULTS += \
  -UDOMAIN_BOUNDS -DINLINE_DOMAINS -DCONSTRAINT_TEMPS -DDISABLE_ENTAILED


clean:
	rm -f $(FDC_OBJS) $(FDC_BINS) $(DFDC_BINS)

fresh:
	rm -f constraints.o values.o
