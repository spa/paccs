#include "paccs.h"

int main(int argc, char *argv[])
{
  int i, j;

  fd_init (&argc, &argv);

  fd_int t = fd_new(1, 3);
  fd_int v = fd_new(1, 3);
  fd_int nsw = fd_new(1, 3);
  fd_int q = fd_new(1, 3);
  fd_int sa = fd_new(1, 3);
  fd_int nt = fd_new(1, 3);
  fd_int wa = fd_new(1, 3);
  int X_INTRODUCED_0[2] = { 1, -1 };

  fd_int va_1[2] = { nsw, v }; fd_poly_ne_k(X_INTRODUCED_0, va_1, 2, 0);
  fd_int va_2[2] = { q, nsw }; fd_poly_ne_k(X_INTRODUCED_0, va_2, 2, 0);
  fd_int va_3[2] = { sa, v }; fd_poly_ne_k(X_INTRODUCED_0, va_3, 2, 0);
  fd_int va_4[2] = { sa, nsw }; fd_poly_ne_k(X_INTRODUCED_0, va_4, 2, 0);
  fd_int va_5[2] = { sa, q }; fd_poly_ne_k(X_INTRODUCED_0, va_5, 2, 0);
  fd_int va_6[2] = { nt, q }; fd_poly_ne_k(X_INTRODUCED_0, va_6, 2, 0);
  fd_int va_7[2] = { nt, sa }; fd_poly_ne_k(X_INTRODUCED_0, va_7, 2, 0);
  fd_int va_8[2] = { wa, sa }; fd_poly_ne_k(X_INTRODUCED_0, va_8, 2, 0);
  fd_int va_9[2] = { wa, nt }; fd_poly_ne_k(X_INTRODUCED_0, va_9, 2, 0);


  if (fd_solve()) {
    fd_println(t);
    fd_println(v);
    fd_println(nsw);
    fd_println(q);
    fd_println(sa);
    fd_println(nt);
    fd_println(wa);
  }
  fd_end();
}

