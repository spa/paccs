% == No, Emacs this is -*-Prolog-*- code, not what you thought... =============

% -- type(NODE, TYPE) ---------------------------------------------------------
%
% type/2: Extract type from AST node.

type(var(_,T,_,_), T) :- type(T).
type(lit(_,T),     T) :- type(T).
type(id(_,T),      T) :- type(T).

type(lit(E,array(T)), array(T)) :- type(E, T).
type(lit(_,T), T) :- type(T).

type([], _).
type([E|Es], T) :- type(E, T), type(Es, T).

% -- type(TYPE) ---------------------------------------------------------------
%
% type/1: generate or test valid types.

type(int).			% integer
type(int(_L,_U)).		% integer interval
type(float).			% floating point
type(float(_L,_U)).		% floating point interval
type(string).			% string
type(array(T,1,_U)) :- type(T).	% array of type

% bound(lb,T,LB)
% bound(ub,T,UB)

bound(lb,int(LB,_UB),LB).
bound(lb,int,1).

bound(ub,int(_LB,UB),UB).
bound(ub,int,999).		% FIXME

