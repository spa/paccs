% == No, Emacs this is -*-Prolog-*- code, not what you thought... =============

:- initialization(main).

main :-
    argument_list(A),
    main(A).

main([ACTION_S|REST]) :- 
    read_term_from_atom(ACTION_S, ACTION, [end_of_term(eof)]),
    action(ACTION), !, main(REST).
main([]).

action(load(F)) :- !, see(F), action(load), seen.
action(load)    :- !, load_ast(AST), g_assign(ast, AST).
action(dump(A)) :- !, dump_ast(A).
action(dump)    :- !, g_read(ast, AST), dump_ast(AST).
action(name)    :- !, 
		   g_read(ast, AST), 
		   sa_names(AST, NAST, ST),
		   g_assign(ast_n, NAST+ST).
action(type)    :- !, 
		   g_read(ast_n, AST+ST), 
		   sa_types(AST, ST),
		   g_assign(ast_t, AST+ST).
action(code)    :- !,
		   g_read(ast_t, AST+ST),
		   cg_emit(AST, ST).
action(halt)    :- !, halt.
action(debug)   :- g_read(ast_debug, true), !, g_assign(ast_debug, false).
action(debug)   :- !, g_assign(ast_debug, true).
action(ACTION)  :- format("%w: unknown action.\n", [ACTION]).


% -----------------------------------------------------------------------------

%% beware: fails if load_ast/3 does not finish with 3rd arg singular list

load_ast(AST) :- g_read(ast_debug, true), !, load_ast_deb(_>[], [], [AST]).
load_ast(AST) :-                             load_ast(_>[], [], [AST]).

load_ast(end_of_file, AST, AST).
load_ast((IN > INT :- MOD), IN, OUT) :-
    call(MOD),
    read(OP), !,
    load_ast(OP, INT, OUT).
load_ast((IN > INT), IN, OUT) :-
    read(OP), !,
    load_ast(OP, INT, OUT).

load_ast_deb(end_of_file, AST, AST).
load_ast_deb((IN > INT :- MOD), IN, OUT) :-
    ( call(MOD) -> format("EXT OK ~q\n", [MOD]) ;
      format("EXT FAIL ~q\n", [MOD]), fail ),
    read(OP), !,
    writeq(IN), write('.'), nl,
    writeq(OP), nl,
    load_ast_deb(OP, INT, OUT).
load_ast_deb(IN > INT,    IN,  OUT) :- 
    read(OP), !,
    writeq(IN), write('.'), nl,
    writeq(OP), nl,
    load_ast_deb(OP, INT, OUT).

% -----------------------------------------------------------------------------

dump_ast(fzn(PREDS, VARS, CONSTRS, SOLVE)) :- 
    !,
    format("AST:\n", []),
    format("  preds:\n", []), PREDS=preds(PS), dump_list(PS),
    format("  vars:\n", []),  VARS=vars(VS), dump_list(VS),
    format("  constrs:\n", []), CONSTRS=constrs(CS), dump_list(CS),
    format("  goal:\n    ~w\n", [SOLVE]).

dump_ast(AST) :- write(AST), nl.

% -----------------------------------------------------------------------------

dump_list([]).
dump_list([I|Is]) :- format("    ~w\n", [I]), dump_list(Is).

% -----------------------------------------------------------------------------
