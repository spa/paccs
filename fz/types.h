// == Prolog AST-stream term parser for FlatZinc ==============================
// 
// (C) 2015 Salvador Abreu
//
// ----------------------------------------------------------------------------

typedef enum { false, true } bool;

typedef union {
    long   ival;
    double rval;
    bool   bval;
    char  *sval;
} lit_token;

typedef char *id_token;

typedef long ast_node;

#define UNION_DEFS() \
    lit_token   lit; \
    id_token    id; \
    ast_node    node
