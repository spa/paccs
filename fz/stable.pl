% == No, Emacs this is -*-Prolog-*- code, not what you thought... =============

% -- Symbol table -------------------------------------------------------------
%
% Open-ended model.

st_lookup(ST,      _, _) :- var(ST), !, fail.
st_lookup([K=V|_], K, V).
st_lookup([_|ST],  K, V) :- st_lookup(ST, K, V).

st_insert(ST,      K, V) :- var(ST), !, ST=[K=V|_].
st_insert([K=_|_], K, _) :- !, throw(duplicate(K)).
st_insert([_|ST],  K, V) :- st_insert(ST, K, V).
