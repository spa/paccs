% == No, Emacs this is -*-Prolog-*- code, not what you thought... =============

% -- utility predicates -------------------------------------------------------

% -- subset (SUB, SET, REST) --------------------------------------------------

subset(L, L, []).
subset(S, [H|T], R) :- subset_aux(T, H, S, R).

   subset_aux(S, R, S, [R]).
   subset_aux([H|T], R1, S, [R1|R]) :- subset_aux(T, H, S, R).
   subset_aux([H|T], X, [X|S], R) :- subset_aux(T, H, S, R).

% -- uniq_name(ROOT, NAME) ----------------------------------------------------

uniq_name(ROOT, NAME) :-
    g_read(ROOT, N),
    N1 is N+1,
    format_to_atom(NAME, "~w_~w", [ROOT, N1]),
    g_assign(ROOT, N1).
